 #include <iostream>
 #include <fcntl.h>
 #include <unistd.h>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <sys/mman.h>

 #include "DatosMemCompartida.h"

int main(int argc, char *argv[])
{	

	char* proyect_bot;
	int fd_bot;
	DatosMemCompartida* ptr_bot;

	float position;

	fd_bot=open("/tmp/file_bot",O_RDWR, 0777);
	if (fd_bot==-1){
		perror("Failed opening bot file");
		}

	proyect_bot=(char*)mmap(NULL,sizeof(ptr_bot),PROT_WRITE|PROT_READ,MAP_SHARED,fd_bot,0);
	if (proyect_bot==MAP_FAILED){
		perror("Failed opening bot file");
		}

	close(fd_bot);

	ptr_bot=(DatosMemCompartida*)proyect_bot;

	while(1)
	{
		position=(ptr_bot->raqueta1.y1+ptr_bot->raqueta1.y2)/2;

		if(ptr_bot->esfera.centro.x > 0)
		{
			if(position<ptr_bot->esfera.centro.y)
				ptr_bot->accion=1;
			else if(position>ptr_bot->esfera.centro.y)
				ptr_bot->accion=-1;
			else
				ptr_bot->accion=0;
		}
		else
		{
			if(position < -4)
				ptr_bot->accion=1;
			else if(position > 4)
				ptr_bot->accion=-1;
			else
				ptr_bot->accion=0;
		}

		usleep(25000);

	}

}