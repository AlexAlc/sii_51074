#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>


int main(int argc, char* argv[]) {
	
	char buff;
	
	if(mkfifo("/tmp/loggerfifo",0777)!=0){
		perror("Creating loogger fifo");
		return 0;
	}

	//Create FIFO in tmp/
	int fd_fifo=open("/tmp/loggerfifo", O_RDONLY);

	if(fd_fifo==-1){
		perror("Open FILE");
		exit(1);
	}

	else{
		while(1){
			char buff;
			int read_fifo = read(fd_fifo,&buff,sizeof(char));
			if ( read_fifo > 0)
				printf("Punto para el jugador %c\n", buff);
			else 
				break;
		}
	}
	close(fd_fifo);
	unlink("/tmp/loggerfifo");
	return 0;
}