//	Author: Alejandro Alcalde Susi

// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
	
	//strcpy(fifo,"/tmp/loggerfifo");
}

CMundo::~CMundo()
{
	if(close(fd_fifo_server_client)==-1){
		perror("Error Closing FIFO server client");
		exit(1);
	};
	unlink("/tmp/serverclientfifo");

	if(close(fd_fifo_client_server)==-1){
		perror("Error Closing FIFO client server");
		exit(1);
	};
	unlink("/tmp/clientserverfifo");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	char buff[200];
	int read_fifo_srv_cl = read(fd_fifo_server_client,&buff,sizeof(buff));
	if ( read_fifo_srv_cl > 0)
		sscanf(buff,"%f %f %f %f %f %f %f %f %f %f %d %d", 
		&esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,
		&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		// if(write(fd_fifo,"2",sizeof(char))==-1){
		// perror("Writing on fifo (id_jug2)");
		// exit(1);
		// }
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		// if(write(fd_fifo,"1",sizeof(char))==-1){
		// perror("Writing on fifo (id_jug1)");
		// exit(1);
		// }
	}

	//Bot action

	ptr_bot->esfera.centro.x=esfera.centro.x;
	ptr_bot->esfera.centro.y=esfera.centro.y;
	ptr_bot->raqueta1.x1=jugador2.x1;
	ptr_bot->raqueta1.x2=jugador2.x2;
	ptr_bot->raqueta1.y1=jugador2.y1;
	ptr_bot->raqueta1.y2=jugador2.y2;

	if(ptr_bot->accion==1)
		OnKeyboardDown('o',0,0);
	else if(ptr_bot->accion==-1)
		OnKeyboardDown('l',0,0);
	else;


}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	sprintf(bufferhilo,"%c",key);
	write(fd_fifo_client_server,bufferhilo,strlen(bufferhilo)+1);
}

void CMundo::Init()
{
	printf("Init\n"); 
//Bot file

	printf("Antes de bot\n");

	fd_bot=open("/tmp/file_bot",O_RDWR|O_CREAT|O_TRUNC, 0777);
	if (fd_bot==-1){
		perror("Failed opening bot file");
	}
	
	write(fd_bot,&bot,sizeof(bot));

	proyect_bot=(char*)mmap(NULL,sizeof(ptr_bot),PROT_WRITE|PROT_READ,MAP_SHARED,fd_bot,0);
	
	close(fd_bot);
	
	ptr_bot=(DatosMemCompartida*)proyect_bot;

	printf("Despues de bot\n");

	//FIFO between Server ---> Client

	unlink("/tmp/serverclientfifo");
	int ret_val=mkfifo("/tmp/serverclientfifo",0777);
	if(ret_val == -1){
		perror("Error Creating FIFO server - client");
		exit(1);
	}

	//Open FIFO between Server ---> Client

	fd_fifo_server_client=open("/tmp/serverclientfifo",O_RDONLY);
	if(fd_fifo_server_client==-1){
		perror("Error Openning FIFO server - client");
		exit(1);
	}

	printf("Open fifo server client\n"); 

	//FIFO between Client ---> Server
	
	unlink("/tmp/clientserverfifo");
	ret_val = mkfifo("/tmp/clientserverfifo",0777);
	if(ret_val == -1){
		perror("Error Creating FIFO client - server");
		exit(1);
	}

	//Open FIFO between Client ---> Server

	fd_fifo_client_server=open("/tmp/clientserverfifo",O_WRONLY);
	printf("%d \n",fd_fifo_client_server);
	if(fd_fifo_client_server==-1){
		perror("Error Openning FIFO client - server");
		exit(1);
	}

	printf("Open fifo client server\n"); 

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	
}
